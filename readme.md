Billing system
===========

Description
------------

**Billing system** is test app.

---------------------------------------

Deployment
---------------------------

Clone source code:

    git clone https://boris_mishunin@bitbucket.org/boris_mishunin/billing_system.git

Open source folder:

    cd billing_system

Start server:

    ./run.sh start 

or

    docker-compose up -d

    
Open in browser http://localhost:8001/

---------------------------------------


Tests
---------------------------

Run tests:

    ./run.sh test

Api links
---------------------------

See on postman https://documenter.getpostman.com/view/11394062/TVYM3FB9