#!/bin/bash

export MODULE_DIR=$(dirname $(readlink -f $0))

start() {

    cd $MODULE_DIR
    docker-compose up -d
}


stop() {
    cd $MODULE_DIR
    docker-compose stop
}


restart() {

    cd $MODULE_DIR
    docker-compose restart
}


test() {
    docker exec -ti bilweb1 sh -c "./manage.py test --settings billing_system.settings api.tests"
}


collect_static() {
    docker exec -ti bilweb1 sh -c "./manage.py collectstatic --noinput"
}

bash() {
    docker exec -ti bilweb1 bash
}

case "$1" in
    build_and_start)
        build_and_start
        ;;
    start)
        start
        ;;

    stop)
        stop
        ;;

    test)
        test
        ;;

    restart)
        restart
        ;;

    collect-static)
        collect_static
        ;;

    bash)
        bash
        ;;
  *)
    echo "Usage: $0 {start|stop|restart|test|collect-static|bash}"
    exit 1
esac
