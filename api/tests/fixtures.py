import factory
from api.models import (
    Wallet, Transaction
)
import random
from django.contrib.auth import get_user_model


class WalletFactory(factory.django.DjangoModelFactory):
    """

        Fixture factory for users wallets.
    """
    user = factory.SubFactory('api.tests.fixtures.UserFactory')

    class Meta:
        model = Wallet
        django_get_or_create = ('user',)


class UserFactory(factory.django.DjangoModelFactory):
    """

        Fixture factory for users.
    """

    username = factory.Sequence(lambda n: f'user {n}')
    password = 'User1234567'
    wallet = factory.RelatedFactory(
        WalletFactory,
        'user'
    )

    class Meta:
        model = get_user_model()


class TransactionFactory(factory.django.DjangoModelFactory):
    """

        Fixture factory for transactions.
    """

    wallet = factory.SubFactory(WalletFactory)
    transaction_reason = factory.Sequence(lambda n: f'Payment {n}')
    amount = random.uniform(1.5, 100.5)

    class Meta:
        model = Transaction
