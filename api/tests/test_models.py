from django.test import TestCase
from api.models import (
    Wallet, Transaction
)
from .fixtures import (
    WalletFactory, TransactionFactory
)


class TestModelsStrMixin(object):
    def test_models_string(self):
        self.assertEqual(str(self.model), self.model_str)


class WalletTestCase(TestCase, TestModelsStrMixin):
    """

        Test case for wallet model.
    """

    def setUp(self):
        WalletFactory()
        self.model = Wallet.objects.last()
        self.model_str = f'Wallet of {self.model.user.username}'


class TransactionTestCase(TestCase, TestModelsStrMixin):
    """

        Test case for transactions model.
    """

    def setUp(self):
        TransactionFactory()
        self.model = Transaction.objects.last()
        self.model_str = (
            f'Transaction wallet- {str(self.model.wallet)}. Reason - {self.model.transaction_reason}'
        )
