from django.test import TestCase
from .fixtures import WalletFactory, TransactionFactory
from rest_framework.test import (
    APIClient
)
from api.models import (
    Wallet, Transaction
)
from api.serializers import (
    WalletSerializer, TransactionSerializer
)
from rest_framework.renderers import JSONRenderer
from json import loads
from django.contrib.auth import get_user_model
import random
from . import get_token


class RequestMock(object):
    user = None

    def __init__(self, user):
        self.user = user


class WalletViewSetTestCase(TestCase):
    """

        Test case for wallets viewset.
    """

    def setUp(self):
        self.wallet1 = WalletFactory()
        TransactionFactory(wallet=self.wallet1, amount=1000.00)
        self.wallet2 = WalletFactory()
        self.user = self.wallet1.user
        self.token = get_token(self.user)
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f"Bearer {self.token}"
        )

    def test_wallets_list(self):
        """

        Tests requests of wallets list

        :return: None
        """

        response = self.client.get(
            '/api/wallets/', format='json'
        )
        wallets_list = Wallet.objects.all()
        wallets = WalletSerializer(
            wallets_list, many=True,
            context={'request': RequestMock(self.wallet1.user)}
        ).data

        wallets_json_string = JSONRenderer().render(wallets)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content),
            loads(wallets_json_string)
        )

    def test_wallet_object(self):
        """

        Tests requests of wallet objects

        :return: None
        """

        response = self.client.get(
            f'/api/wallets/{self.wallet1.id}/', format='json'
        )
        wallet = WalletSerializer(
            self.wallet1,
            context={'request': RequestMock(self.wallet1.user)}
        ).data

        wallet_json_string = JSONRenderer().render(wallet)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content),
            loads(wallet_json_string)
        )

    def test_get_my_wallet(self):
        """

        Tests requests of wallet objects

        :return: None
        """

        response = self.client.get(
            '/api/get-my-wallet/', format='json'
        )
        wallet = WalletSerializer(
            self.wallet1,
            context={'request': RequestMock(self.wallet1.user)}
        ).data

        wallet_json_string = JSONRenderer().render(wallet)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content),
            loads(wallet_json_string)
        )

    def test_fill_wallet(self):
        """

        Tests requests of wallet fills

        :return: None
        """

        old_amount = self.wallet1.wallet_amount

        amount = 10.5

        response = self.client.post(
            '/api/fill-wallet/', data={"amount": amount}, format='json'
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.wallet1.wallet_amount-old_amount, amount)

        # test no amount in param
        response = self.client.post(
            '/api/fill-wallet/', data={}, format='json'
        )
        self.assertEqual(response.status_code, 400)

    def test_transfer_money(self):
        """

        Tests requests for money transfers

        :return: None
        """

        old_amount = self.wallet1.wallet_amount

        amount = 10.5

        response = self.client.post(
            f'/api/transfer-money/{self.wallet2.id}/', data={"amount": amount}, format='json'
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(old_amount-self.wallet1.wallet_amount, amount)
        self.assertEqual(self.wallet2.wallet_amount, amount)

        # test no money
        response = self.client.post(
            f'/api/transfer-money/{self.wallet2.id}/', data={"amount": 10000.00}, format='json'
        )
        self.assertEqual(response.status_code, 400)

        # test no amount in param
        response = self.client.post(
            f'/api/transfer-money/{self.wallet2.id}/', data={}, format='json'
        )
        self.assertEqual(response.status_code, 400)


class TransactionViewSetTestCase(TestCase):
    """

        Test case for wallets transaction views.
    """

    def setUp(self):
        self.wallet = WalletFactory()
        TransactionFactory(amount=1000.00)
        transactions = [TransactionFactory(
            wallet=self.wallet, amount=random.uniform(-1.5, 1.5)
        ) for i in range(5)]
        self.transaction = transactions[0]
        self.user = self.wallet.user
        self.token = get_token(self.user)
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f"Bearer {self.token}"
        )

    def test_transactions_list(self):
        """

        Tests requests of transactions list

        :return: None
        """

        response = self.client.get(
            '/api/transactions/', format='json'
        )
        transaction_list = Transaction.objects.filter(
            wallet=self.wallet
        ).order_by('created')
        transaction = TransactionSerializer(
            transaction_list, many=True,
        ).data

        transactions_json_string = JSONRenderer().render(transaction)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content),
            loads(transactions_json_string)
        )

    def test_transaction_object(self):
        """

        Tests requests of transaction object

        :return: None
        """

        response = self.client.get(
            f'/api/transactions/{self.transaction.id}/', format='json'
        )
        transaction = TransactionSerializer(
            self.transaction,
        ).data

        transaction_json_string = JSONRenderer().render(transaction)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content),
            loads(transaction_json_string)
        )


class SignUpViewTestCase(TestCase):
    """

        Test case for sign up view.
    """

    def setUp(self):
        self.client = APIClient()
        self.post_data = {
            "username": 'test_user',
            "password": 'User123456',
        }

    def test_sign_up(self):
        """

            Tests sign up

            :return: None
        """

        response = self.client.post(
            '/api/signup/', data=self.post_data, format='json'
        )
        self.assertEqual(response.status_code, 201)
        is_user_exists = get_user_model().objects.filter(
            username=self.post_data.get('username')
        ).exists()
        self.assertTrue(is_user_exists)
