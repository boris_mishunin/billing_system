from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.db.models import Sum


class Wallet(models.Model):
    """

        This model contains wallets info
    """

    user = models.OneToOneField(
        User,
        related_name='wallet',
        on_delete=models.DO_NOTHING,
        verbose_name='user'
    )

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'wallets'
        verbose_name = 'Wallet'
        verbose_name_plural = 'Wallets'

    @property
    def wallet_amount(self):
        q = Transaction.objects.filter(wallet=self).aggregate(wallet_amount=Sum('amount'))
        return q.get('wallet_amount') if q.get('wallet_amount') is not None else 0

    def __str__(self):
        return f'Wallet of {self.user.username}'


class Transaction(models.Model):
    """

        This model contains transactions likes info
    """

    wallet = models.ForeignKey(
        Wallet,
        on_delete=models.CASCADE,
        verbose_name='Wallet'
    )

    transaction_reason = models.TextField(verbose_name='Transaction reason')

    amount = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name='Transaction amount'
    )

    created = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = 'wallet_transactions'
        verbose_name = 'Wallet transactions'
        verbose_name_plural = 'Wallet transactions'

    def __str__(self):
        return f'Transaction wallet- {str(self.wallet)}. Reason - {self.transaction_reason}'
