from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Wallet, Transaction


class UserSerializer(serializers.ModelSerializer):
    """

        Serializer for user.
    """

    password = serializers.CharField(write_only=True)

    class Meta:
        model = get_user_model()
        fields = ("id", "username", "password")

    def create(self, validated_data):
        user = get_user_model().objects.create(
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        Wallet.objects.create(user=user)
        return user


class WalletSerializer(serializers.ModelSerializer):
    """

        Serializer for wallets.
    """

    user = UserSerializer(read_only=True)

    class Meta:
        model = Wallet
        fields = ('id', 'user')

    def to_representation(self, instance):
        repr = super(WalletSerializer, self).to_representation(instance)
        if instance == self.context['request'].user.wallet:
            repr['wallet_amount'] = instance.wallet_amount
        return repr


class TransactionSerializer(serializers.ModelSerializer):
    """

        Serializer for transactions.
    """

    class Meta:
        model = Transaction
        fields = ('id', 'wallet', 'transaction_reason', 'amount', 'created')
