from django.urls import path
from .views import (
    SignUpUserView, WalletViewSet, TransactionsViewSet
)
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt import views as jwt_views


urlpatterns = [
    path('signup/', SignUpUserView.as_view(), name='user-sign-up'),
    path('token/', TokenObtainPairView.as_view(), name='get-token'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(),
         name='token-refresh'),
    path('wallets/', WalletViewSet.as_view({
        'get': 'list',
    }), name='wallets'),
    path('wallets/<int:pk>/', WalletViewSet.as_view({
        'get': 'retrieve',
    }), name='wallet'),
    path('get-my-wallet/', WalletViewSet.as_view({
        'get': 'get_my_wallet',
    }), name='my-wallet'),
    path('fill-wallet/', WalletViewSet.as_view({
        'post': 'fill_wallet',
    }), name='fill-wallet'),
    path('transfer-money/<int:pk>/', WalletViewSet.as_view({
        'post': 'transfer_money',
    }), name='fill-wallet'),
    path('transactions/', TransactionsViewSet.as_view({
        'get': 'list',
    }), name='transactions'),
    path('transactions/<int:pk>/', TransactionsViewSet.as_view({
        'get': 'retrieve',
    }), name='transaction'),
]
