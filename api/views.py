from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model
from .serializers import (
    UserSerializer, WalletSerializer, TransactionSerializer
)
from .models import Wallet, Transaction
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action
from django.db import transaction


class SignUpUserView(CreateAPIView):
    model = get_user_model()
    permission_classes = [permissions.AllowAny]
    serializer_class = UserSerializer


class WalletViewSet(viewsets.ReadOnlyModelViewSet):
    """

        This view set for wallet operations.
    """

    serializer_class = WalletSerializer
    queryset = Wallet.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    @action(['post'], detail=False)
    def get_my_wallet(self, request):
        return Response(self.get_serializer(request.user.wallet).data)

    @action(['post'], detail=False)
    def fill_wallet(self, request):
        wallet = request.user.wallet
        data = dict(request.data)
        data['transaction_reason'] = 'Fill user wallet'
        data['wallet'] = wallet.pk
        ts = TransactionSerializer(data=data)
        if ts.is_valid():
            ts.save()
        else:
            return Response(ts.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(self.get_serializer(wallet).data)

    @transaction.atomic
    @action(['post'], detail=True)
    def transfer_money(self, request, pk):
        reciver_wallet = self.get_object()
        sender_wallet = request.user.wallet
        amount = request.data.get('amount')
        if amount is None:
            return Response({"amount": "This field is required."}, status=status.HTTP_400_BAD_REQUEST)

        if sender_wallet.wallet_amount < amount:
            return Response({"amount": "Not enough money on wallet."}, status=status.HTTP_400_BAD_REQUEST)

        data = {}
        data['transaction_reason'] = request.data.get(
            'transaction_reason', 'Send money to' + reciver_wallet.user.username
        )
        data['wallet'] = sender_wallet
        data['amount'] = -amount
        Transaction.objects.create(**data)

        data['transaction_reason'] = request.data.get(
            'transaction_reason', 'Received money from' + request.user.username
        )
        data['wallet'] = reciver_wallet
        data['amount'] = amount
        Transaction.objects.create(**data)
        return Response(self.get_serializer(sender_wallet).data)


class TransactionsViewSet(viewsets.ReadOnlyModelViewSet):
    """

        This view set return wallets transactions.
    """

    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Transaction.objects.filter(
            wallet=self.request.user.wallet
        ).order_by('created')
